import pytz
import datetime

def parse_month(month):# pragma: no cover
    if month == 'January':
        return 1
    elif month == 'February':
        return 2
    elif month == 'March':
        return 3
    elif month == 'April':
        return 4
    elif month == 'May':
        return 5
    elif month == 'June':
        return 6
    elif month == 'July':
        return 7
    elif month == 'August':
        return 8
    elif month == 'September':
        return 9
    elif month == 'October':
        return 10
    elif month == 'November':
        return 11
    elif month == 'December':
        return 12

def parse_date(date):# pragma: no cover
    timezone = pytz.timezone('Asia/Jakarta')
    input_date = date.split(' ')
    date = datetime.date(int(input_date[2]), parse_month(input_date[1]), int(input_date[0]))
    return date