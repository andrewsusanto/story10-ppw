from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.index, name="homepage"),
    path('auth/', views.auth, name="authentication_page"),
    path('logout/', views.logout_user, name="logout"),
    path('api/v1/login/', views.api_login, name="login_endpoint"),
    path('api/v1/signup/', views.api_sign_up, name="sign_up_endpoint"),
    # path('api/v1/edit/', views.api_edit, name="edit_data_endpoint"),
]