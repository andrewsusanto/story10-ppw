from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import os
import time
import random
import string

from faker import Faker

class LoginPageUnitTest(TestCase):
    def test_redirect_to_login_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)
    
    def test_bad_request_api(self):
        response = Client().get('/api/v1/login/')
        self.assertEqual(response.status_code, 400)

        response = Client().get('/api/v1/signup/')
        self.assertEqual(response.status_code, 400)

class LoginPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(LoginPageFunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super(LoginPageFunctionalTest, self).tearDown()
    
    def test_login_page_login(self):
        # Create User
        User.objects.create_user('andrew', 'admin@admin.com', 'corona2020')

        self.browser.get(self.live_server_url + '/')

        # Wait until page open 
        time.sleep(5)

        username = self.browser.find_element_by_xpath('//input[@name="username"]')
        username.send_keys("andrew")

        password = self.browser.find_element_by_xpath('//input[@name="password"]')
        password.send_keys("corona2020")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'Log In')][1]")
        login_button.click()

        # Wait Authentication process and redirect to protected homepage
        time.sleep(5)
        self.assertIn('andrew', self.browser.page_source) # Verify username is showed

        edit_button = self.browser.find_element_by_xpath('//span[@id="editButton"]')
        edit_button.click()

        name = self.browser.find_element_by_xpath('//input[@name="name"]')
        name.send_keys("Andrew Susanto")

        tanggal_lahir = self.browser.find_element_by_xpath('//input[@name="birth_date"]')
        tanggal_lahir.send_keys('1 January 1990')

        id_line = self.browser.find_element_by_xpath('//input[@name="line_id"]')
        id_line.send_keys('iniidlinesaya')

        save_button = self.browser.find_element_by_xpath('//button[contains(.,"Save Data")]')
        save_button.click()

        time.sleep(5)
        self.assertIn('Data saved', self.browser.page_source)

        self.browser.get(self.live_server_url + '/')
        self.assertIn('Andrew Susanto', self.browser.page_source)
        self.assertIn('1 January 1990', self.browser.page_source)
        self.assertIn('iniidlinesaya', self.browser.page_source)

        logout_button = self.browser.find_element_by_xpath('//button[contains(.,"Logout")]')
        logout_button.click()

        time.sleep(5)
        self.assertNotIn('andrew', self.browser.page_source) # Verify logout

    def test_login_page_login_no_user(self):
        self.browser.get(self.live_server_url + '/')

        # Wait until page open 
        time.sleep(5)

        username = self.browser.find_element_by_xpath('//input[@name="username"]')
        username.send_keys("andrew")

        password = self.browser.find_element_by_xpath('//input[@name="password"]')
        password.send_keys("corona2020")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'Log In')][1]")
        login_button.click()

        time.sleep(5)

        # Wait Authentication process and redirect to protected homepage
        self.assertIn('Username is not registered, please check and try again', self.browser.page_source) # Verify error message is showed

    def test_login_page_wrong_password(self):
        # Create User
        User.objects.create_user('andrew', 'admin@admin.com', 'corona2020')

        self.browser.get(self.live_server_url + '/')

        # Wait until page open 
        time.sleep(5)

        username = self.browser.find_element_by_xpath('//input[@name="username"]')
        username.send_keys("andrew")

        password = self.browser.find_element_by_xpath('//input[@name="password"]')
        password.send_keys("corona20201")

        login_button = self.browser.find_element_by_xpath("//button[contains(.,'Log In')][1]")
        login_button.click()

        time.sleep(5)

        self.assertIn('Username / password not match, please check and try again', self.browser.page_source)

    def test_create_account(self):
        self.browser.get(self.live_server_url + '/')

        # Wait until page open 
        time.sleep(5)

        sign_up_button = self.browser.find_element_by_xpath('//a[contains(.,"Create an account")]')
        sign_up_button.click()

        time.sleep(5)

        # Fill details
        username = self.browser.find_element_by_xpath('//input[@name="s_username"]')
        username.send_keys("andrew")

        email = self.browser.find_element_by_xpath('//input[@name="s_email"]')
        email.send_keys("andrew@gmail.com")

        password = self.browser.find_element_by_xpath('//input[@name="s_password"]')
        password.send_keys("corona2020")

        # Sign up
        sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Sign Up")]')
        sign_up_button.click()

        time.sleep(10)

        user = authenticate(username="andrew", password="corona2020")
        self.assertTrue(user) # User is not None

        sign_up_button = self.browser.find_element_by_xpath('//a[contains(.,"Create an account")]')
        sign_up_button.click()

        time.sleep(5)

        # TEST DUPLICATE USERNAME
        username = self.browser.find_element_by_xpath('//input[@name="s_username"]')
        username.clear()
        username.send_keys("andrew")

        email = self.browser.find_element_by_xpath('//input[@name="s_email"]')
        email.clear()
        email.send_keys("test@gmail.com")

        password = self.browser.find_element_by_xpath('//input[@name="s_password"]')
        password.clear()
        password.send_keys("corona2021")

        # Sign up
        sign_up_button = self.browser.find_element_by_xpath('//button[contains(.,"Sign Up")]')
        sign_up_button.click()

        time.sleep(5)

        self.assertIn('Username is used, please use another username', self.browser.page_source)
