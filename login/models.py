from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserData(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=128)
    birth_date = models.DateField()
    line_id = models.CharField(max_length=64)
    photo_url = models.TextField()

    def __str__(self):
        return self.name
