from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import firebase_admin
from firebase_admin import credentials
from firebase_admin import storage

from login.models import UserData
from login.helper import *
import json
import pytz
import os
import secrets

# Create your views here.
def index(request):
    if request.user.is_authenticated:
        context = {
            'user': request.user
        }
        if request.method == 'POST':
            try: 
                user_data = UserData.objects.get(user=request.user)
            except:
                user_data = UserData()
                user_data.user = request.user

            user_data.name = request.POST['name']
            user_data.birth_date = parse_date(request.POST['birth_date'])
            user_data.line_id = request.POST['line_id']
            
            if 'photo' in request.FILES and request.FILES['photo']:
                cred = credentials.Certificate(settings.FIREBASE_CRED_PATH)
                firebase_admin.initialize_app(cred,{
                    'storageBucket': 'login-story10.appspot.com'
                })
                bucket = storage.bucket()

                image_data = request.FILES['photo'].read()
                blob = bucket.blob(secrets.token_urlsafe(64)+ "-" + request.FILES['photo'].name)
                blob.upload_from_string(
                        image_data,
                        content_type='image/jpg'
                    )
                blob.make_public()
                user_data.photo_url = blob.public_url
            
            user_data.save()
            context['message'] = 'Data saved'

        try:
            user_data = UserData.objects.get(user=request.user)
        except:
            user_data = ''

        context['user_data'] =user_data
        
        return render(request, 'index.html', context)
    else:
        return redirect('/auth/')

def auth(request):
    return render(request, 'login.html')

def logout_user(request):
    logout(request)
    return redirect('/auth/')

@csrf_exempt
def api_login(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        if 'username' in data and 'password' in data:
            user = authenticate(request, username=data['username'], password=data['password'])

            if user is not None:
                login(request, user)
                return JsonResponse({
                    'status': 200,
                    'message': 'Successful login'
                })
            else :
                try:
                    User.objects.get(username=data['username'])
                    return JsonResponse({
                        'status' : 401,
                        'message': 'Username / password not match, please check and try again'
                    })
                except User.DoesNotExist:
                    return JsonResponse({
                        'status' : 401,
                        'message': 'Username is not registered, please check and try again'
                    })

    return HttpResponseBadRequest()

@csrf_exempt
def api_sign_up(request):
    if request.method == 'POST':
        data = json.loads(request.body)

        if 'username' in data and 'password' in data and 'email' in data:
            try:
                User.objects.create_user(data['username'], data['email'], data['password'])
                return JsonResponse({
                    'status': 200,
                    'message': 'Successfully create account, please login to continue'
                })
            except:
                return JsonResponse({
                    'status' : 500,
                    'message': 'Username is used, please use another username'
                })
    return HttpResponseBadRequest()

# @csrf_exempt
# def api_edit(request):
#     if request.method == 'POST' and request.user.is_authenticated:
#         data = json.loads(request.body)

#         if all(k in data for k in ['name', 'birth_date', 'line_id']):
#             try: 
#                 user_data = UserData.objects.get(user=request.user)
#             except:
#                 user_data = UserData()
#                 user_data.user = request.user

#             user_data.name = data['name']
#             user_data.birth_date = parse_date(data['birth_date'])
#             user_data.line_id = data['line_id']
#             user_data.photo_url = data['photo_url']
#             user_data.save()

#             return JsonResponse({
#                 'status' : 200,
#                 'message': 'Data saved'
#             })

#     return HttpResponseBadRequest()
